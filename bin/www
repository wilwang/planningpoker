#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('../app');
var debug = require('debug')('planningpoker-server:server');
var http = require('http');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3001');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);
var io = require('socket.io').listen(server);


/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


/**
 * register a client connection on socket.io
 * TODO :: move this stuff into its own module
 */
let sessionStore = require('../lib/sessionStore');
let connectedSockets = {};

io.on('connection', function(socket){
  let sessionid = socket.handshake.query.sessionid;
  let participant = socket.handshake.query.participant;
  let session = sessionStore.getSession(sessionid);

  if (participant && session) {
    session = sessionStore.addParticipantToSession(sessionid, participant);
    connectedSockets[socket.id] = { sessionid: sessionid, participant: participant };
  }

  socket.join(sessionid);
  io.sockets.in(sessionid).emit('playerJoinLeave', session);

  socket.on('disconnect', function() {
    let socketid = socket.id;
    let sessioninfo = connectedSockets[socketid];

    if (sessioninfo != null && sessioninfo != undefined && sessioninfo != 'undefined') {
      let session = sessionStore.getSession(sessioninfo.sessionid);
      
      if (session) {
        session = sessionStore.removeParticipantFromSession(sessioninfo.sessionid, sessioninfo.participant);
        socket.broadcast.to(sessioninfo.sessionid).emit('playerJoinLeave', session);
      }
    }
  });

  socket.on('playerScoreSubmit', function(data) {
    let session = sessionStore.getSession(data.sessionid);
    
    if (session) {
      sessionStore.scoreSession(data.sessionid, data.participant, data.score);
      socket.broadcast.to(data.sessionid).emit('scoreSubmitted', session);
    }
  });

  socket.on('sessionReset', function(sessionid) {
    let session = sessionStore.resetSession(sessionid);

    if (session) {
      io.sockets.in(sessionid).emit('resetClient', session);;
    }
  });
});
