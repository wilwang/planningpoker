'use strict';

let uuid = require('uuid');

function getParticipantIndex(session, participant) {
    let idx = session.participants.map(function (p) { return p.participant; }).indexOf(participant);

    return idx;
}

let sessionStore = {
    __store: [],

    createSession: function(host) {
        let session = {
            id: uuid.v1(),
            participants: [],
            host: host
        };

        this.__store.push(session);

        return session;
    },

    getAllSessions: function () {
        return this.__store;
    },

    getSession: function (id) {
        let sessions = this.__store.filter(function (s) { return s.id === id; });

        if (sessions.count === 0)
            return null;

        return sessions[0];
    },

    saveSession: function (session) {
        let sessionIds = this.__store.map(function (s) { return s.id; });
        let sessionIndex = sessionIds.indexOf(session.id);

        if (sessionIndex === -1) {
            this.__store.push(session);
        } else {
            this.__store[sessionIndex] = session;
        }

        return session;        
    },

    participantExistsInSession: function (sessionid, participant) {
        let session = this.getSession(sessionid);
        
        if (!session)
            return false;

        let idx = getParticipantIndex(session, participant);

        return idx > -1;
    },

    addParticipantToSession: function (sessionid, participant) {
        let session = this.getSession(sessionid);
        let idx = getParticipantIndex(session, participant);

        if (idx === -1)
            session.participants.push({ id: uuid.v1(), participant: participant, score: null });

        return session;
    },

    removeParticipantFromSession: function (sessionid, participant) {
        let session= this.getSession(sessionid);
        let idx = getParticipantIndex(session, participant);

        if (idx > -1)
            session.participants.splice(idx, 1);
        
        return session;
    },

    scoreSession: function (sessionid, participant, score) {
        let session = this.getSession(sessionid);
        let idx = getParticipantIndex(session, participant);

        session.participants[idx].score = score;

        return session;
    },

    resetSession: function (sessionid) {
        let session = this.getSession(sessionid);
        session.participants.map(function(p) {p.score = null;});
        
        return session;
    },

    clear: function() {
        this.__store.length = 0;
    }
};

module.exports  = sessionStore;