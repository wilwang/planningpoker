(function () {
    let sessionid = $('#sessionid').text();
    let socket = io('http://localhost:3001/', {query: 'sessionid=' + sessionid}); 

    socket.on('playerJoinLeave', function(session) {
        let participantcount = session ? session.participants.length : 0;
        $('#participantcount').text(participantcount);

        updateBoard(session);
    });

    socket.on('scoreSubmitted', function(session) {
        updateBoard(session);
    });

    socket.on('resetClient', function(session) {
        $('.player-score').hide();
        updateBoard(session);
    });

    $('#btnReset').click(function() {
        socket.emit('sessionReset', sessionid);
    });

    let updateBoard = function(session) {
        let updateArea = $('#participants');
        updateArea.empty();

        if (session != null && session != undefined) {
            session.participants.map(function (p) {
                let playerDiv = $('<div>', { id: p.id + '_card', class: 'player col-xs-4 text-center'});
                let playerName = $('<p>', { id: p.id + '_participant' });;
                let playerScore = $('<p>', { id: p.id + '_score', class: 'player-score' });
                
                playerDiv.append(playerName);
                playerDiv.append(playerScore);
                updateArea.append(playerDiv);

                playerName.html(p.participant);
                playerScore.html(p.score);
            });

            let unscoreds = session.participants.filter( function (p) { return p.score === null });
            
            if (unscoreds.length === 0)
                $('.player-score').show();
        }
    }
})();