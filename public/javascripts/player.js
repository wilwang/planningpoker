(function () {
    let sessionid = $('#sessionid').text();
    let participant = $('#participant').text();
    let socket = io('http://localhost:3001/', {query: 'sessionid=' + sessionid + '&participant=' + participant}); 

    $('#btnScore').click(function() {
        let btn = $('#btnScore');

        if (btn.attr('disabled')) {
            return;
        }

        let score = $('#scoreInput').val();

        let payload = {
            sessionid: sessionid,
            participant: participant,
            score: score
        };

        socket.emit('playerScoreSubmit', payload);

        $('#btnScore').attr('disabled', true);
    });

    socket.on('playerJoinLeave', function(session) {
        let participantcount = session ? session.participants.length : 0;
        $('#participantcount').text(participantcount);
    });

    socket.on('resetClient', function(session) {
        $('#btnScore').attr('disabled', false);
    });

})();