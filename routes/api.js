let express = require('express');
let router = express.Router();
let sessionStore = require('../lib/sessionStore');

// get all sessions
router.get('/sessions', function(req, res, next) {
  res.send(sessionStore.getAllSessions());
});

// get session by id
router.get('/sessions/:sessionid', function (req, res, next) {
  let id = req.params['sessionid'];
  let session = sessionStore.getSession(id);

  if (!session) {
    return res.status(404).send({ error: 'session ' + id + ' not found' });
  }

  return res.send(session);
});

// create a new session
router.post('/sessions', function (req, res, next) {
  let host = req.body.hostuser;
  let session = sessionStore.createSession(host);

  res.send(session);
});

// participant joins a session
router.post('/sessions/:sessionid', function (req, res, next) {
  let id = req.params['sessionid'];
  let participant = req.body.participant;

  session = sessionStore.addParticipantToSession(id, participant);

  res.send(session);
});

// participant scores an issue
router.put('/sessions/:sessionid/:participant', function (req, res, next) {
  let sessionid = req.params['sessionid'];
  let participant = req.params['participant'];
  let score = req.body.score;

  session = sessionStore.scoreSession(sessionid, participant, score);

  res.send(session);
});

module.exports = router;