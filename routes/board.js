let express = require('express');
let router = express.Router();
let sessionStore = require('../lib/sessionStore');

router.post('/', function(req, res, next) {
    let host = req.body.username;
    let session = sessionStore.createSession(host);

    res.render('board', { session: session });
});

module.exports = router;