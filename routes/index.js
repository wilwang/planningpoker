var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/host', function (req, res, next) {
  res.render('hostSignIn');
});

router.get('/participant', function (req, res, next) {
  var sessionid = req.query['sessionid'] || '';
  res.render('participantSignIn', { sessionid: sessionid });
});

module.exports = router;
