let express = require('express');
let router = express.Router();
let sessionStore = require('../lib/sessionStore');

router.post('/', function(req, res, next) {
    let sessionid = req.body.sessionid;
    let participant = req.body.participant;
    let session = sessionStore.getSession(sessionid);

    if (!session) {
        return res.render('participantSignIn', {sessionid: ''});
    }

    let nameExists = sessionStore.participantExistsInSession(sessionid, participant)
    if (nameExists) {
        return res.render('participantSignIn', { sessionid: sessionid, nameExists: true });
    }

    return res.render('player', { session: session, participant: participant });
});

module.exports = router;