let assert = require('assert');

describe('sessionStore', function() {
    let sessionStore = require('../lib/sessionStore');

    beforeEach(function() {
        sessionStore.clear();
    });

    describe('#createSession(host)', function() {
        it ('should create session with host set from parameter', function() {
            let session = sessionStore.createSession('iamhost');
            assert.ok(session);
            assert.equal(session.host, 'iamhost');
        });

        it ('should save session to __store resulting in count = 1', function() {
            let session = sessionStore.createSession('youarehost');
            assert.equal(sessionStore.__store.length, 1);
        });
    });

    describe('#getAllSessions()', function() {
        it ('should return all session in the store', function() {
            sessionStore.createSession('dolores');
            sessionStore.createSession('mauve');
            sessionStore.createSession('teddy');

            assert.equal(sessionStore.getAllSessions().length, 3);
        });
    });

    describe('#getSession(id)', function() {
        it ('should return the session if the session id exists', function() {
            let session = sessionStore.createSession('dolores');
            let returnedSession = sessionStore.getSession(session.id);

            assert.equal(session.id, returnedSession.id);
        });

        it ('should return null if the session id does not exist', function () {
            let session = sessionStore.createSession('dolores');
            let returnedSession = sessionStore.getSession('thisshouldnotexist');

            assert.equal(returnedSession, null);
        });
    });

    describe ('#saveSession(session)', function() {
        it ('should save a session if it exists', function() {
            let session = sessionStore.createSession('teddy');

            assert.equal(session.host, 'teddy');

            session.host = 'dolores';
            sessionStore.saveSession(session);

            let returnedSession  = sessionStore.getSession(session.id);

            assert.equal(session.host, 'dolores');
        });

        it ('should insert a session if session does not exist', function() {
            assert.equal(sessionStore.getAllSessions().length, 0);

            sessionStore.saveSession({
                id: 'abc',
                host: 'mauve'
            });

            assert.equal(sessionStore.getAllSessions().length, 1);
        });
    });

    describe('#participantExistsInSession(session, participant)', function() {
        it ('should return true when participant already exists in session', function() {
            let session = sessionStore.createSession('temauveddy');
            session = sessionStore.addParticipantToSession(session.id, 'felix');

            let exists = sessionStore.participantExistsInSession(session.id, 'felix');

            assert.equal(exists, true);
        });

        if ('should return false when participant does not exist in session', function() {
            let session = sessionStore.createSession('mauve');
            session = sessionStore.addParticipantToSession(session.id, 'felix');

            let exists = sessionStore.participantExistsInSession(session.id, 'theotherguy');

            assert.equal(exists, false);
        });
    });

    describe('#addParticipantToSession(sessionid, participant)', function() {
        it ('should add a participant to session with null score', function() {
            let session = sessionStore.createSession('teddy');
            session = sessionStore.addParticipantToSession(session.id, 'felix');

            assert.equal(session.participants[0].participant, 'felix');
            assert.equal(session.participants[0].score, null);
        });
    });

    describe('#removeParticipantFromSession(sessionid, participant)', function() {
        it ('should remove a participant if the participant exists in the session', function() {
            let session = sessionStore.createSession('teddy');
            session = sessionStore.addParticipantToSession(session.id, 'felix');

            let part = session.participants[0];
            
            assert.equal(part.participant, 'felix');

            session = sessionStore.removeParticipantFromSession(session.id, 'felix');

            assert.equal(session.participants.length, 0);
        });
    });

    describe('#scoreSession(sessionid, participantid, score)', function() {
        it ('should update score for the participant in the session', function() {
            let session = sessionStore.createSession('teddy');
            session = sessionStore.addParticipantToSession(session.id, 'felix');

            let part = session.participants[0];

            assert.equal(part.participant, 'felix');
            assert.equal(part.score, null);

            session = sessionStore.scoreSession(session.id, 'felix', 20);

            part = session.participants[0];

            assert(part.score, 20); 
        });
    });

    describe('#clear()', function() {
        it ('should clear all sessions in the store', function() {
            sessionStore.createSession('dolores');
            sessionStore.createSession('mauve');
            sessionStore.createSession('teddy');
            sessionStore.clear()
            
            assert.equal(sessionStore.getAllSessions().length, 0);
        });
    });
});